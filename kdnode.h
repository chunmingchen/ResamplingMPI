#ifndef KDNODE_H
#define KDNODE_H

#include <vector>
#include <algorithm>

struct KDNode{
  int extent[6];
  KDNode *left, *right;
  KDNode(): left(NULL), right(NULL) {}

  // recursively construct the KD tree
  // nparts: number of partitions to be generated
  // valid_pts: will compute internally, but is used just for debug print
  KDNode(std::vector<int> &global_hist, int global_dim[3], int extent_[6], int nparts, bool printInfo=false, int valid_pts=0)
    : left(NULL), right(NULL)
  {
    std::copy(extent_, extent_+6, extent);
    if (nparts <= 1) {
      if (printInfo)
        printf("Leaf extent: %d %d, %d %d, %d %d, valid points: %d\n", extent[0], extent[1], extent[2], extent[3], extent[4], extent[5], valid_pts);
      return;
    } else {
      if (printInfo)
        printf("extent: %d %d, %d %d, %d %d, nparts: %d\n", extent[0], extent[1], extent[2], extent[3], extent[4], extent[5], nparts);
    }

    int dim = find_max_idx(extent[1]-extent[0], extent[3]-extent[2], extent[5]-extent[4]);
    int leftext = extent[dim*2];
    int rightext = extent[dim*2+1];
    if (rightext <= leftext) {
      printf("Warning: KD-partitioning: Wrong extent input or too many input partitions.  Will generate less number of given partitions.\n");
      return;
    }
    // generate 1d histogram
    int npts = 0;
    std::vector<int> hist1d(rightext-leftext+1, 0);
    {
      int p[3]; int &z = p[2], &y = p[1], &x = p[0];
      for (z = 0; z <= extent[5]-extent[4]; z++)
        for (y = 0; y <= extent[3]-extent[2]; y++)
          for (x = 0; x <= extent[1]-extent[0]; x++)
          {
            int id = x+extent[0] + global_dim[0]*(y+extent[2] + global_dim[1]*(z+extent[4]));
            hist1d[p[dim]] += global_hist[id];
            npts+=global_hist[id];
            id++;
          }
    }
    // fidn cut idx
    int cut_idx;
    int left_pts=0;
    {
      int pts = 0;
      int cut_pts = (nparts / 2) * npts / nparts;
      for (cut_idx=leftext; cut_idx<=rightext; cut_idx++)
      {
        int i = cut_idx-leftext;
        pts += hist1d[i];
        if (pts > cut_pts) {
          left_pts = pts - hist1d[i];
          break;
        }
      }
    }
    int left_parts = std::min(nparts-1, std::max(1,
                        (int)round(left_pts*nparts/(double)npts)));
    if (printInfo)
    {
      printf("hist1d: ");
      for (int i=0; i<hist1d.size(); i++)
        printf("%d ", hist1d[i]);
      printf(". npts=%d=[%d+%d]. parts [%d:%d]\n", npts, left_pts, npts-left_pts, left_parts, nparts-left_parts);
    }

    // subdivide
    for (int i=0; i<2; i++)
    {
      int sub_extent[6];
      std::copy(extent, extent+6, sub_extent);
      if (i==0) {
        sub_extent[dim*2+1] = cut_idx;
      } else {
        sub_extent[dim*2] = cut_idx+1;
      }
      std::vector<int> new_hist;
      for (int z=sub_extent[4]; z<=sub_extent[5]; z++)
        for (int y = sub_extent[2]; y<=sub_extent[3]; y++)
          for (int x = sub_extent[0]; x<=sub_extent[1]; x++)
          {
            int id = x + global_dim[0]*( y + global_dim[1] * z );
            new_hist.push_back(global_hist[id]);
          }
      if (i==0)
        left = new KDNode(global_hist, global_dim, sub_extent, left_parts, printInfo, left_pts );
      else
        right = new KDNode(global_hist, global_dim, sub_extent, nparts - left_parts, printInfo, npts-left_pts );

    }
  }

  ~KDNode() {
    if (left) delete left;
    if (right) delete right;
  }

  int find_max_idx(int a, int b, int c) {
    if (a>=b && a>=c) return 0;
    if (b>=a && b>=c) return 1;
    return 2;
  }

#if 0 // debug
  int outputExtent(Grids &grids, int id) {
    vsp_new(vtkCubeSource, cube);
    if (!left && !right) {
      double bounds[6];
      bounds[0] = grids.origin[0]+grids.grid_width*extent[0];
      bounds[1] = grids.origin[0]+grids.grid_width*extent[1];
      bounds[2] = grids.origin[1]+grids.grid_width*extent[2];
      bounds[3] = grids.origin[1]+grids.grid_width*extent[3];
      bounds[4] = grids.origin[2]+grids.grid_width*extent[4];
      bounds[5] = grids.origin[2]+grids.grid_width*extent[5];
      cube->SetCenter(bounds[0]*.5+bounds[1]*.5,
          bounds[2]*.5+bounds[3]*.5,
          bounds[4]*.5+bounds[5]*.5);
      cube->SetXLength(bounds[1]-bounds[0]);
      cube->SetYLength(bounds[3]-bounds[2]);
      cube->SetZLength(bounds[5]-bounds[4]);
      cube->Update();
      std::stringstream ss;
      ss << "box_" << id << ".vtp" ;
      vsp_new(vtkXMLPolyDataWriter, writer);
      writer->SetFileName(ss.str().c_str());
      writer->SetInputData(cube->GetOutput());
      writer->Write();

      id++;
    }

    if (left) id = left->outputExtent(grids, id);
    if (right) id = right->outputExtent(grids, id);
    return id;
  }
#endif

  // depth-first search of node in given rank
  int dfs(int rank, KDNode **ppnode)
  {
    if (rank<0)
      return -1;
    if (left == NULL && right==NULL) { // leaf
      if (rank==0)
        *ppnode = this;
      return rank-1;
    }
    rank = left->dfs(rank, ppnode);
    rank = right->dfs(rank, ppnode);
    return rank;
  }

  // a dfs traversal
  int getAllExtents(std::vector<std::vector<int> > &extent, int grid_width,
                    int *global_ext, int ghost, int id)
  {
    if (left == NULL && right == NULL) { // leaf node
      extent[id] = std::vector<int>(6, 0);
      extent[id][0] = this->extent[0]*grid_width;
      extent[id][1] = std::min((this->extent[1]+1)*grid_width -1 + ghost, global_ext[1]);
      extent[id][2] = this->extent[2]*grid_width;
      extent[id][3] = std::min((this->extent[3]+1)*grid_width -1 + ghost, global_ext[3]);
      extent[id][4] = this->extent[4]*grid_width;
      extent[id][5] = std::min((this->extent[5]+1)*grid_width -1 + ghost, global_ext[5]);

      id ++;
    } else {
      id = left->getAllExtents(extent, grid_width, global_ext, ghost, id);
      id = right->getAllExtents(extent, grid_width, global_ext, ghost, id);
    }
    return id;
  }
}; // KDNode



#endif // KDNODE_H

