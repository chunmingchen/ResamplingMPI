#include <iostream>

#include "cp_time.h"
#include "parallel_resample.h"
#include "kdnode.h"

using namespace std;

#define vsp_new(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

ParallelResample::ParallelResample()
: DebugWriteOutputData(false),
  DebugVerbose(true),
  HistBinWidth(5),
  bUseVTKProbeFilter(false),
  GhostWidth(1),
  DIYPrefix("./DIY.XXXXXX"),
  DIYInMemory(-1),
  DIYThreads(1)
{
}

vtkSmartPointer<vtkImageData> ParallelResample::resample(vtkSmartPointer<vtkDataSet> data, Grids &outputGrids)
{
  cout << "Input number of points: " << data->GetNumberOfPoints() << ", number of cells: " << data->GetNumberOfCells() << endl;

  // create grids
  vsp_new(vtkImageData, image);
  image->SetOrigin(outputGrids.origin[0], outputGrids.origin[1], outputGrids.origin[2]);
  image->SetSpacing(outputGrids.grid_width, outputGrids.grid_width, outputGrids.grid_width);
  image->SetDimensions(outputGrids.dim[0], outputGrids.dim[1], outputGrids.dim[2]);
  //image->SetExtent(0, outputGrids.dim[0]-1, 0, outputGrids.dim[1]-1, 0, outputGrids.dim[2]-1);
  image->AllocateScalars(VTK_UNSIGNED_CHAR, 1);

  // where to output
  vsp_new(vtkImageData, output_data);

  Timer timer;
  timer.start();
  if (this->bUseVTKProbeFilter) { // much slower
    vsp_new(vtkProbeFilter, probe);
    probe->SetInputData(image);
    probe->SetSourceData(data);
    probe->Update();
    output_data->DeepCopy(vtkImageData::SafeDownCast( probe->GetOutput() ) );
  } else {

    vsp_new(vtkImageProbeFilter, probe);
    probe->SetInputData(image);
    probe->SetSourceData(data);
    probe->Update();

    output_data->DeepCopy( vtkImageData::SafeDownCast( probe->GetOutput() ) );
    if (output_data.Get()==NULL)
      cout << "data not image data" << endl;
  }

  timer.end();
  cout << "Time: " << (double)timer.getElapsedUS()/1000. << " ms" << endl;

  return output_data;
}

vector<int> ParallelResample::compute_local_histogram(
    vtkSmartPointer<vtkImageData> data,
    Grids &grids)
{
  char *maskArray = static_cast<char *>(
        data->GetPointData()->GetArray("vtkValidPointMask")->GetVoidPointer(0));
  // number of 3D bins
  int n = grids.dim[0]*grids.dim[1]*grids.dim[2];
  vector<int> hist(n, 0);
  for (int i=0; i<data->GetNumberOfPoints(); i++)
    if (maskArray[i]) {
      double point[3];
      data->GetPoint(vtkIdType(i), point);
      int p[3];
      for (int j=0; j<3; j++) {
        p[j] = (point[j]-grids.origin[j])/grids.grid_width;
        assert(p[j] >= 0 && p[j] < grids.dim[j]);
      }
      int idx = p[0] + grids.dim[0] * (p[1] + grids.dim[1] * p[2]);
      hist[ idx ] ++;
    }

  return hist;
}

//////////////////////////////////////
/// DIY related functions

/// add local block information into master
/// Returns the block ids for the current rank (currently only one block is involved)
std::vector<int> init_diy_blocks(diy::Master &master, diy::mpi::communicator &world, vector<vector<int> > allExtents )
{
  const int partitions = allExtents.size();

  diy::ContiguousAssigner   assigner(world.size(), partitions);
  std::vector<int> gids;
  assigner.local_gids(world.rank(), gids);

  assert(gids.size()==1);
  int gid = gids[0];

  Block *b = new Block();
  std::copy(allExtents[gid].begin(), allExtents[gid].end(), b->extent);
  diy::Link *l = new diy::Link();
  for (int i=0; i<partitions; i++)
  {
    // add block information for later point send
    diy::BlockID neighbor;
    neighbor.gid = i;
    neighbor.proc = assigner.rank(neighbor.gid);
    l->add_neighbor(neighbor);
  }
  master.clear();
  master.add(gid, b, l);
  return gids;
}

void* create_block()
{
  Block* b = new Block;
  return b;
}

void destroy_block(void* b)
{
  delete static_cast<Block*>(b);
}

void save_block(const void* b, diy::BinaryBuffer& bb)
{
  // not sure whether this is correct, but it is only used when doing out of core:
  diy::save(bb, *static_cast<const Block*>(b));
}

void load_block(void* b, diy::BinaryBuffer& bb)
{
  // not sure whether this is correct, but it is only used when doing out of core:
  diy::load(bb, *static_cast<Block*>(b));
}

// used in diy Master::foreach
struct SendPointInfo{
  vector<vector<int> > *pAllExtents ;
  vtkSmartPointer<vtkImageData> data;
  vtkSmartPointer<vtkImageData> result;
};

void ParallelResample::init_result(
    vtkSmartPointer<vtkImageData> data,
    vtkSmartPointer<vtkImageData> result,
    vector<int> ext, Grids &global_grids)
{
  vtkDataSetAttributes::FieldList* PointList;
  PointList = new vtkDataSetAttributes::FieldList(1);
  PointList->InitializeFieldList(data->GetPointData());

  int dim[3];
  dim[0] = ext[1]-ext[0]+1;
  dim[1] = ext[3]-ext[2]+1;
  dim[2] = ext[5]-ext[4]+1;

  vtkIdType numPts = dim[0]*dim[1]*dim[2];

  // First, copy the input to the output as a starting point
  result->CopyStructure(data);

  result->SetOrigin(
      global_grids.origin[0] + ext[0]*global_grids.grid_width,
      global_grids.origin[1] + ext[2]*global_grids.grid_width,
      global_grids.origin[2] + ext[4]*global_grids.grid_width);
  result->SetSpacing(global_grids.grid_width, global_grids.grid_width, global_grids.grid_width);
  result->SetDimensions(dim[0], dim[1], dim[2]);

  // Allocate storage for output PointData
  // All input PD is passed to output as PD. Those arrays in input CD that are
  // not present in output PD will be passed as output PD.
  vtkPointData* outPD = result->GetPointData();
  outPD->CopyAllocate((*PointList), numPts, numPts);

  outPD->SetNumberOfTuples(numPts);
  //outPD->GetArray(0)->GetDataTypeSize();

  // fill zeros to all arrays
  for (int i=0; i<outPD->GetNumberOfArrays(); i++)
    for (int j=0; j<outPD->GetArray(i)->GetNumberOfComponents(); j++)
      outPD->GetArray(i)->FillComponent(j,0);

  // If not filling zeros to all arrays, at least you should fill vtkValidPointMask:
  //   outPD->GetArray("vtkValidPointMask")->FillComponent(0,0);

  //result->Print(cout);

  delete PointList;
}

typedef std::pair<int, float> IdValuePair;

// used in diy Master::foreach
// distribute local points to other nodes based on the partitioning
void send_points(void *b_, const diy::Master::ProxyWithLink &cp, void *pSendPointInfo_)
{
  SendPointInfo *pInfo = static_cast<SendPointInfo *>(pSendPointInfo_);

  Block *b = static_cast<Block *> (b_);

  char *maskArray = static_cast<char *>(
        pInfo->data->GetPointData()->GetArray("vtkValidPointMask")->GetVoidPointer(0));

  int i,j,k, id=0;
  int *dim = pInfo->data->GetDimensions();

  vector<vector<IdValuePair> > msgList(pInfo->pAllExtents->size());

  char *resultValidMask = (char *)pInfo->result->GetPointData()->GetArray("vtkValidPointMask")->GetVoidPointer(0);

  int npts = 0; // number of points to send

  // for each point
  for (k=0; k<dim[2]; k++)
    for (j=0; j<dim[1]; j++)
      for (i=0; i<dim[0]; i++, id++)
      {
        if (maskArray[id]==0)
          continue;
        // find block id for the point
        for (int bid = 0; bid < pInfo->pAllExtents->size(); bid++)
        {
          vector<int> &ext = pInfo->pAllExtents->at(bid);
          if ( i >= ext[0] && i <= ext[1] &&
               j >= ext[2] && j <= ext[3] &&
               k >= ext[4] && k <= ext[5])
          {
            int rdim[3] = {ext[1]-ext[0]+1, ext[3]-ext[2]+1, ext[5]-ext[4]+1};
            int result_id = (i-ext[0]) + rdim[0] * ( (j-ext[2]) + rdim[1] * (k-ext[4]));
            std::pair<int, float> id_value(result_id, pInfo->data->GetPointData()->GetArray(0)->GetTuple1(id)) ;

            if (bid == cp.gid()) // self block
            {
              assert(id_value.first < pInfo->result->GetNumberOfPoints());
              // directly add the point into result
              pInfo->result->GetPointData()->GetArray(0)->SetTuple1(vtkIdType(id_value.first), id_value.second);
              resultValidMask[id_value.first] = 1;

            } else
            {
              msgList[bid].push_back(id_value);
              //printf("send to [%d]: <%d , %f>\n", bid, id_value.first, id_value.second);
              npts++;
            }
          } // if in extent

        } // for bid
      } // for i
  diy::Link *l = cp.link();
  for (i=0; i<msgList.size(); i++)
  {
    vector<IdValuePair> &msg = msgList[i];
    cp.enqueue(l->target(i), msg);  // DIY will not send empty message

    if (msg.size())
      printf("Enqueue %d -> %d: %zu points\n", cp.gid(), i, msg.size());
  }
  //printf("(%d) Number of points to send: %d\n", cp.gid(), npts);
}

void recv_points(void *b_, const diy::Master::ProxyWithLink &cp, void *pSendPointInfo_)
{
  SendPointInfo *pInfo = static_cast<SendPointInfo *>(pSendPointInfo_);

  char *validMask = (char *)pInfo->result->GetPointData()->GetArray("vtkValidPointMask")->GetVoidPointer(0);

  int npts = 0; // number of points recv'ed

  std::vector<int> in;
  cp.incoming(in);
  for (unsigned i = 0; i < in.size(); i++)
  {
    vector<IdValuePair> msg;
    cp.dequeue(in[i], msg);

    for (int j = 0; j< msg.size(); j++) {
      IdValuePair inpair = msg[j];
      // printf("recv from [%d] : <%d , %f>\n", i, inpair.first, inpair.second);

      pInfo->result->GetPointData()->GetArray(0)->SetTuple1(vtkIdType(inpair.first), inpair.second);
      validMask[inpair.first] = 1;

      npts++;
    }
  }

  printf("(%d) Number of points received: %d\n", cp.gid(), npts);

}


void ParallelResample::saveAllBounds(std::vector<std::vector<int> > &allExtents, Grids &grids)
{
  for (int i=0; i<allExtents.size(); i++)
  {
    vector<int> &extent = allExtents[i];
    double bounds[6];
    bounds[0] = grids.origin[0]+grids.grid_width*extent[0];
    bounds[1] = grids.origin[0]+grids.grid_width*extent[1];
    bounds[2] = grids.origin[1]+grids.grid_width*extent[2];
    bounds[3] = grids.origin[1]+grids.grid_width*extent[3];
    bounds[4] = grids.origin[2]+grids.grid_width*extent[4];
    bounds[5] = grids.origin[2]+grids.grid_width*extent[5];
    vsp_new(vtkCubeSource, cube);
    cube->SetCenter(bounds[0]*.5+bounds[1]*.5,
        bounds[2]*.5+bounds[3]*.5,
        bounds[4]*.5+bounds[5]*.5);
    cube->SetXLength(bounds[1]-bounds[0]);
    cube->SetYLength(bounds[3]-bounds[2]);
    cube->SetZLength(bounds[5]-bounds[4]);
    cube->Update();
    std::stringstream ss;
    ss << "box_" << i << ".vtp" ;
    vsp_new(vtkXMLPolyDataWriter, writer);
    writer->SetFileName(ss.str().c_str());
    writer->SetInputData(cube->GetOutput());
    writer->Write();

  }
}
//////////////////////////////////

vtkSmartPointer<vtkImageData> ParallelResample::run(
    diy::mpi::communicator &world,
    vtkSmartPointer<vtkDataSet> local_data,
    Grids &outputGrids)
{
  int rank = world.rank();

  Timer timer;
  timer.start();

  /// local serial resample
  vtkSmartPointer<vtkImageData>
      local_resampled_data = resample(local_data, outputGrids);

  printf("Sampled number of points: %lld\n", local_resampled_data->GetNumberOfPoints());

  timer.end();

  printf("[%d] Done resampling (s): %f\n", rank, timer.getElapsedMS()/1000.);

#ifdef PROFILING
  world.barrier();  if (rank==0) cout << "== Barrier ==" << endl;
#endif


  if (this->DebugWriteOutputData) {
    vsp_new(vtkXMLImageDataWriter, writer);
    stringstream ss;  ss << "local_" << rank << ".vti";
    writer->SetFileName(ss.str().c_str());
    writer->SetInputData(local_resampled_data);
    writer->Write();
  }

  /// create coarsen grids for local histogram computation
  Grids coarseGrids ;
  coarseGrids.grid_width = outputGrids.grid_width*HistBinWidth;
  std::copy(outputGrids.origin, outputGrids.origin+3, coarseGrids.origin);
  coarseGrids.dim[0] = (int)ceil(outputGrids.dim[0]/(double)HistBinWidth);
  coarseGrids.dim[1] = (int)ceil(outputGrids.dim[1]/(double)HistBinWidth);
  coarseGrids.dim[2] = (int)ceil(outputGrids.dim[2]/(double)HistBinWidth);

  /// compute local histogram
  vector<int> local_hist =
    compute_local_histogram(local_resampled_data, coarseGrids);

  if (0)
  {
    printf("local histogram of rank %d: ", rank);
    for (int i=0; i<local_hist.size(); i++)
      if (local_hist[i]>0)
        printf("%d:%d ", i,local_hist[i]);
    printf("\n");
  }

  /// gather global histograms
  vector<int> global_hist(local_hist.size(), 0);
  diy::mpi::all_reduce(world, local_hist, global_hist, std::plus<int>() );

  timer.end();
  printf("[%d] Done gathering global histograms (s): %f\n", rank, timer.getElapsedMS()/1000.);

#ifdef PROFILING
  world.barrier();  if (rank==0) cout << "== Barrier ==" << endl;
#endif

  if (rank==0 && this->DebugVerbose)
  {
    printf("global histogram of rank %d: ", rank);
    for (int i=0; i<global_hist.size(); i++)
      if (global_hist[i]>0)
        printf("%d:%d ", i,global_hist[i]);
    printf("\n");
  }
  //allReduce(world, threads, local_resampled_data, coarseGrids)

  int partitions = world.size();

  /// kd-tree partitioning
  int global_extent[6] = {0, coarseGrids.dim[0]-1, 0, coarseGrids.dim[1]-1, 0, coarseGrids.dim[2]-1};
  KDNode *root = new KDNode(global_hist, coarseGrids.dim, global_extent, partitions, rank==0, 0);

  /// compute extents of each node, with ghost
  vector<vector<int> > allExtents(partitions);
  int output_extent[6] = {0, outputGrids.dim[0]-1, 0, outputGrids.dim[1]-1, 0, outputGrids.dim[2]-1};
  root->getAllExtents(allExtents, HistBinWidth, output_extent, GhostWidth, 0);

  timer.end();
  printf("[%d] Done KD partitioning (s): %f\n", rank, timer.getElapsedMS()/1000.);

#ifdef PROFILING
  world.barrier();  if (rank==0) cout << "== Barrier ==" << endl;
#endif

  // debug: export bounds
#if 1
  if (rank==0 && this->DebugWriteOutputData) {
    saveAllBounds(allExtents, outputGrids);
  }
#endif

  /////////////////////////////////////////
  /// Redistributing points

  /// [Master initialization]
  diy::FileStorage          storage(DIYPrefix);
  diy::Master               master(world,
                                   DIYThreads,
                                   DIYInMemory,
                                   &create_block,
                                   &destroy_block,
                                   &storage,
                                   &save_block,
                                   &load_block);

  /// create local diy blocks, return block ids (currently only one is created)
  vector<int> gids = init_diy_blocks(master, world, allExtents);

  /// initialize output, assign local points if in the output bounds
  vsp_new(vtkImageData, output);
  init_result(local_resampled_data, output, allExtents[gids[0]], outputGrids);

  /// exchange
  SendPointInfo spInfo;
  spInfo.data = local_resampled_data;
  spInfo.pAllExtents = &allExtents;
  spInfo.result = output;
  master.foreach(&send_points, &spInfo);


  timer.end();
  printf("[%d] Done preparing exhange (s): %f\n", rank, timer.getElapsedMS()/1000.);

#ifdef PROFILING
  world.barrier();  if (rank==0) cout << "== Barrier ==" << endl;
#endif

  master.exchange();

  timer.end();
  printf("[%d] Done exhange (s): %f\n", rank, timer.getElapsedMS()/1000.);

  master.foreach(&recv_points, &spInfo);

  timer.end();
  printf("[%d] Done recv points (s): %f\n", rank, timer.getElapsedMS()/1000.);


  // output result
  stringstream ss;
  ss << "result_" << rank << ".vti";
  vsp_new(vtkXMLImageDataWriter, writer);
  writer->SetFileName(ss.str().c_str());
  writer->SetInputData(spInfo.result);
  writer->Write();

  if (this->DebugVerbose)
    spInfo.result->Print(cout);


  timer.end();
  printf("[%d] Done writing %s (s): %f\n", rank, ss.str().c_str(), timer.getElapsedMS()/1000.);

  return output;

}
