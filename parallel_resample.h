#ifndef PARALLEL_RESAMPLE_H
#define PARALLEL_RESAMPLE_H

#include <vector>
#include <string>

#include "vtkSmartPointer.h"
#include "vtkXMLPolyDataReader.h"
#include "vtkImageData.h"
#include "vtkCellArray.h"
#include "vtkPolyDataWriter.h"
#include "vtkPolyDataReader.h"
#include "vtkSmartPointer.h"

#include "vtkUnstructuredGrid.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkStructuredGrid.h"
#include <vtkRectilinearGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkDataSetAttributes.h>
#include <vtkDataObject.h>

#include "vtkImageProbeFilter.h"
#include "vtkProbeFilter.h"
#include "vtkDataSetTriangleFilter.h"

#include "vtkCubeSource.h"
#include "vtkXMLPolyDataWriter.h"
#include "vtkXMLImageDataWriter.h"

#include "cp_time.h"
#include "kdnode.h"

#include <diy/master.hpp>
#include <diy/link.hpp>
#include <diy/reduce.hpp>
#include <diy/partners/all-reduce.hpp>
#include <diy/partners/swap.hpp>
#include <diy/assigner.hpp>

struct Grids{
  double origin[3];
  double grid_width;
  int dim[3];
};

struct Block
{
  int      extent[6];
  std::vector<float>   val;
};

namespace diy
{
  template<>
  struct Serialization<Block>
  {
    static void save(BinaryBuffer& bb, const Block& b)
    { diy::save(bb, b); diy::save(bb, b); }

    static void load(BinaryBuffer& bb, Block& b)
    { diy::load(bb, b); diy::load(bb, b); }
  };
}

class ParallelResample{

  vtkSmartPointer<vtkImageData> resample(vtkSmartPointer<vtkDataSet> data, Grids &outputGrids);

  std::vector<int> compute_local_histogram(vtkSmartPointer<vtkImageData> data, Grids &grids);

  void saveAllBounds(std::vector<std::vector<int> > &allExtents, Grids &grids);

  void init_result(
      vtkSmartPointer<vtkImageData> data,
      vtkSmartPointer<vtkImageData> result,
      std::vector<int> ext, Grids &global_grids);

public:

  bool DebugWriteOutputData;
  bool DebugVerbose;
  int HistBinWidth; // how many sample points per dimension forms a histogram bin.  Larger is coarser
  bool bUseVTKProbeFilter;
  int GhostWidth;
  std::string DIYPrefix;
  int DIYInMemory;  // maximum blocks to store in memory
  int DIYThreads;

  ParallelResample();

  vtkSmartPointer<vtkImageData> run(
      diy::mpi::communicator &world,
      vtkSmartPointer<vtkDataSet> local_data,
      Grids &outputGrids);
};



#endif // PARALLEL_RESAMPLE_H

