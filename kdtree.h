#ifndef KDTREE_H
#define KDTREE_H

#include <mpi.h>

#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkCharArray.h>

#include <diy/mpi.hpp>
#include <diy/master.hpp>
#include <diy/assigner.hpp>
#include <diy/serialization.hpp>
#include <diy/io/block.hpp>
#include <diy/reduce.hpp>
#include <diy/mpi.hpp>
#include <diy/types.hpp>

#include <vtkSmartPointer.h>
#include <vtkImageData.h>

int kdtree(diy::mpi::environment &env, diy::mpi::communicator &world,
          vtkSmartPointer<vtkImageData> data ,
           int hist, diy::ContinuousBounds global_bounds);


#endif // KDTREE_H
