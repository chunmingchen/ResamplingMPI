#include <string>
#include <cstdio>
#include <iostream>
#include <vector>
#include <map>
#include <iomanip>

#include "vtkSmartPointer.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkObjectFactory.h"
#include "vtkXMLPolyDataReader.h"
#include "vtkAxes.h"
#include "vtkAxesActor.h"
#include "vtkImageData.h"
#include <vtkExtractEdges.h>
#include "vtkVector.h"
#include "vtkTriangle.h"
#include "vtkTriangleFilter.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkQuadricClustering.h"
#include "vtkPLYReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkQuadric.h"
#include "vtkPolyDataReader.h"
#include "vtkSmartPointer.h"
#include "vtkDataSetMapper.h"
#include "vtkOutlineFilter.h"
#include "vtkXMLImageDataReader.h"
#include "vtkThreshold.h"
#include "vtkStructuredGridOutlineFilter.h"

#include <vtkProperty.h>
#include "vtkMultiBlockPLOT3DReader.h"
#include "vtkAppendFilter.h"
#include "vtkUnstructuredGrid.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkStructuredGrid.h"
#include <vtkRectilinearGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkDataSetAttributes.h>
#include <vtkDataObject.h>

#include "vtkImageProbeFilter.h"
#include "vtkProbeFilter.h"
#include "vtkDataSetTriangleFilter.h"

#include "vtkCubeSource.h"
#include "vtkXMLPolyDataWriter.h"
#include "vtkXMLImageDataWriter.h"

#include "vtkPolyDataMapper.h"
#include "vtkRenderWindow.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkMatrix4x4.h"
#include "vtkMatrix3x3.h"

#include <diy/master.hpp>
#include <diy/link.hpp>
#include <diy/reduce.hpp>
#include <diy/partners/all-reduce.hpp>
#include <diy/partners/swap.hpp>
#include <diy/assigner.hpp>

#include "cp_time.h"
#include "opts.h"
#include "parallel_resample.h"

using namespace std;
using namespace opts;

#define vsp_new(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

/////////////////////////////////////
vector<vtkSmartPointer<vtkDataSet> > dataList;
vector<vtkSmartPointer<vtkImageData> > outputList;

void run(diy::mpi::communicator &world);

//int                       nblocks   = 1
int                       threads   = 1;
int                       in_memory = -1;
std::string               data_path = DATA_PATH;
std::string               prefix    = "./DIY.XXXXXX";
int               bin_width    = 5;  // histogram bin width
int               ghost_width = 1;  // ghost border width
int grid_division = 100;
Grids outputGrids;
/////////////////////////////////////

///////////////////////////////////////
/// for Demo
vtkRenderer *ren;
vtkRenderWindow *renWin;

bool bShowGrids = false;
bool bShowOriginal = true;
bool bShowOutput = false;
int world_size = 1;
int load_blocks = 1;
//////////////////////////////////////


void get_intersect_idx(double bmin, double bmax, double origin, double stepsize, int steps, int &minidx, int &nidx)
{
  nidx = 0;
  if (bmin>origin+steps*stepsize || bmax < origin)
    return;
  for (int i=0; i<steps; i++)
  {
    double x = origin+i*stepsize ;
    if (x>bmax) {
      minidx = i-nidx;
      break;
    }
    if (x>=bmin)
      nidx++;
  }
}

///
/// determine grid resolution for resampling
/// max_grid_division: max number of subregions to divide each dimension
///                    Each divided blocks has the same width per dimension
///                    Note that it will generate (max_grid_division+1) grids for the longest dimension of the input
///
Grids set_origin_and_dim(double *bounds, int max_grid_division)
{
  Grids outputGrids;
  double *origin = outputGrids.origin;
  int *dim = outputGrids.dim;

  double res[3];
  for (int i=0; i<3; i++)
      res[i] = (bounds[i*2+1]-bounds[i*2])/max_grid_division;
  outputGrids.grid_width = std::max(res[0], std::max(res[1], res[2]));

  double one_over_grid_width = 1. / outputGrids.grid_width;

  dim[0] = min((int)ceil((bounds[1]-bounds[0])*one_over_grid_width)+1, max_grid_division);
  dim[1] = min((int)ceil((bounds[3]-bounds[2])*one_over_grid_width)+1, max_grid_division);
  dim[2] = min((int)ceil((bounds[5]-bounds[4])*one_over_grid_width)+1, max_grid_division);
  origin[0] = bounds[0];
  origin[1] = bounds[2];
  origin[2] = bounds[4];
  return outputGrids;
}

////////////////////////////////////////
/// For demo
void draw()
{
    ren->RemoveAllViewProps();

    // draw input data
    if (bShowOriginal)
    {
      for (int i=0; i<world_size; i++)
      {
        vsp_new(vtkPolyDataMapper, mapper);
        vtkStructuredGrid *sgrid = vtkStructuredGrid::SafeDownCast(dataList[i].Get());
        if (sgrid) // is structured grids
        {
          vsp_new(vtkStructuredGridOutlineFilter, outline);
          outline->SetInputData( dataList[i] );
          outline->Update();
          mapper->SetInputData( outline->GetOutput() );
        } else
        {
          vsp_new(vtkOutlineFilter, outline);
          outline->SetInputData( dataList[i] );
          outline->Update();
          mapper->SetInputData( outline->GetOutput() );
        }

        vsp_new(vtkActor, polyactor);
        polyactor->SetMapper(mapper);

        ren->AddActor(polyactor);
      }
    }

    // draw output data
    if (bShowOutput)
    {
      for (int i=0; i<world_size; i++)
      {
        {
          // outline
          vsp_new(vtkOutlineFilter, outline);
          outline->SetInputData( outputList[i] );
          outline->Update();

          vsp_new(vtkPolyDataMapper, mapper);
          mapper->SetInputData( outline->GetOutput() );

          vsp_new(vtkActor, polyactor);
          polyactor->SetMapper(mapper);
          polyactor->GetProperty()->SetColor(1,1,0);

          ren->AddActor(polyactor);
        }

        // transparent thresholds
        {
          vsp_new(vtkThreshold, threshold);
          threshold->SetInputData(outputList[i]);
          threshold->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, "vtkValidPointMask");
          threshold->ThresholdByUpper(0.5);
          threshold->Update();

          vsp_new(vtkDataSetMapper, mapper);
          mapper->SetInputData( threshold->GetOutput() );

          vsp_new(vtkActor, actor);
          actor->SetMapper(mapper);
          actor->GetProperty()->SetRepresentationToSurface();
          actor->GetProperty()->SetOpacity(.5);

          ren->AddActor(actor);
        }
      }
    }


    // axes
    vsp_new(vtkAxesActor, axes);
    ren->AddActor(axes);

    if (bShowGrids)
    {

        vsp_new(vtkImageData, image);
        image->SetDimensions(outputGrids.dim[0]+1, outputGrids.dim[1]+1, outputGrids.dim[2]+1); // grids are larger than cells by one in each dimension
        image->SetOrigin(outputGrids.origin[0], outputGrids.origin[1], outputGrids.origin[2]);
        image->SetSpacing(outputGrids.grid_width, outputGrids.grid_width, outputGrids.grid_width);

        vsp_new(vtkDataSetMapper, mapper);
        mapper->SetInputData(image);

        vsp_new(vtkActor, polyactor);
        polyactor->SetMapper(mapper);
        polyactor->GetProperty()->SetRepresentationToWireframe();

        ren->AddActor(polyactor);
    }

    ren->SetBackground(0,0,.5); // Background color
    renWin->Render();
}

// Define interaction style
class KeyPressInteractorStyle : public vtkInteractorStyleTrackballCamera
{
  public:
    static KeyPressInteractorStyle* New();
    vtkTypeMacro(KeyPressInteractorStyle, vtkInteractorStyleTrackballCamera);

    virtual void OnKeyPress()
    {
      // Get the keypress
      vtkRenderWindowInteractor *rwi = this->Interactor;
      std::string key = rwi->GetKeySym();

      // Output the key that was pressed
      std::cout << "Pressed " << key << std::endl;

      // Handle an arrow key
      if(key == "Up")
        {
        std::cout << "The up arrow was pressed." << std::endl;
        }

      // Handle a "normal" key
      if(key == "s")
        {
          bShowOutput = !bShowOutput;
          bShowOriginal = !bShowOriginal ;
          draw();

        }

      if (key == "o" )
      {
          bShowOriginal = !bShowOriginal ;
          bShowOutput = !bShowOutput;
          draw();
      }
#if 0
      if (key=="minus") {
          if (grid_division>1)
            grid_division /= 2;
          cout << "grid division =" << grid_division << endl;
          if (bShowGrids)
            draw();
      }
      if (key=="plus") {
          grid_division *= 2;
          cout << "grid division =" << grid_division << endl;
          if (bShowGrids)
            draw();
      }
#endif
      if (key=="g") {
          bShowGrids = ! bShowGrids;
          draw();
      }

      // Forward events
      vtkInteractorStyleTrackballCamera::OnKeyPress();
    }

};
vtkStandardNewMacro(KeyPressInteractorStyle);


////////////////////////////////////////////////////////

vtkSmartPointer<vtkDataSet> load_input(string filepath, int id, int load_blocks, bool gridsOnly = false)
{
  // If we want to load multiple blocks, convert them into unstructured grids
  if (load_blocks > 1) {
    vsp_new(vtkAppendFilter, append);
    for (int i=0; i<load_blocks; i++)
    {
      append->AddInputData(load_input(filepath, id*load_blocks+i, 1, gridsOnly));
    }
    append->Update();
    return vtkDataSet::SafeDownCast( append->GetOutput() );
  }

  // convert to unstructured
  if (filepath.find("35001")!= string::npos) {
    stringstream ss1, ss2;
    string gfile, qfile;
    if (id < 12) {
      ss1 << filepath << "/s35_noinj.r1b" << (id+1) << ".p3d.g";
      gfile = ss1.str();
      ss2 << filepath << "/s35_noinj.r1b" << (id+1) << ".p3d.q35001";
      qfile = ss2.str();
    } else if (id < 12 + 36) {
      ss1 << filepath << "/s35_noinj.r2b" << (id-12+1) << ".p3d.g35001";
      gfile = ss1.str();
      ss2 << filepath << "/s35_noinj.r2b" << (id-12+1) << ".p3d.q35001";
      qfile = ss2.str();
    } else {
      ss1 << filepath << "/s35_noinj.r3b" << (id-12-36+1) << ".p3d.g";
      gfile = ss1.str();
      ss2 << filepath << "/s35_noinj.r3b" << (id-12-36+1) << ".p3d.q35001";
      qfile = ss2.str();
    }

    printf("Loading %s and %s\n", gfile.c_str(), qfile.c_str());

    vsp_new(vtkMultiBlockPLOT3DReader, reader);
    reader->SetXYZFileName(gfile.c_str());
    if (!gridsOnly)
      reader->SetQFileName(qfile.c_str());
    reader->AddFunction(100);
    reader->AddFunction(110);
    reader->AddFunction(200);
    reader->SetAutoDetectFormat(1);
    reader->SetScalarFunctionNumber(110);
    reader->SetVectorFunctionNumber(200);
    reader->Update();

    //append->AddInputData(reader->GetOutput()->GetBlock(0));
    return vtkDataSet::SafeDownCast( reader->GetOutput()->GetBlock(0) );
    }

  //append->Update();
  //data = vtkSmartPointer<vtkUnstructuredGrid>::New();
  //data->DeepCopy(append->GetOutput());
  printf("File path %s not loaded!!\n", filepath.c_str());
  return NULL;

}

////////////////////////////////////

//////////////////////////////////////


int main( int argc, char **argv )
{
  diy::mpi::environment     env(argc, argv);
  diy::mpi::communicator    world;

  Options ops(argc, argv);
  ops
      //>> Option('b', "blocks",  nblocks,        "number of blocks")
      >> Option('t', "thread",  threads,        "number of threads")
      >> Option('m', "memory",  in_memory,      "maximum blocks to store in memory")
      >> Option(     "prefix",  prefix,         "prefix for external storage")
      >> Option('d', "data",    data_path,      "Data path")
      >> Option(     "bw",      bin_width,      "histogram bin width in grids")
      >> Option('g', "gd",      grid_division,  "grid division")
      >> Option(     "load_blocks", load_blocks,"Blocks to load in dataset 35001")
  ;

  if (ops >> Present('h', "help", "show help"))
  {
    if (world.rank() == 0)
        std::cout << ops;

    return 1;
  }

  world_size = world.size();

  int rank = world.rank();
  if (rank==0) {
    printf("Note: Use argument -h to see input parameters\n");
  }

  // load data => save to global variable data
  vtkSmartPointer<vtkDataSet> data = load_input(data_path, rank, load_blocks);

  // get user bounds
#if 1
  double user_bounds[6]={-0.201, 0.211, -0.51, 0.51, -0.51, 0.51}; // whole domain
  //double bounds[6]={-0.084, 0.092, -0.51, 0.51, -0.51, 0.51}; // only the rotor
#else
  // use global bounds as user bounds
  double user_bounds[6];
  vector<double> global_bounds(6);
  {
    vector<double> local_bounds(6);
    std::copy(data->GetBounds(), data->GetBounds()+6, local_bounds.begin());
    // prepare for all_reduce
    local_bounds[0] = -local_bounds[0];
    local_bounds[2] = -local_bounds[2];
    local_bounds[4] = -local_bounds[4];
    diy::mpi::all_reduce(world, local_bounds, global_bounds, diy::mpi::maximum<double>() );
    global_bounds[0] = -global_bounds[0];
    global_bounds[2] = -global_bounds[2];
    global_bounds[4] = -global_bounds[4];
    printf("Global bounds: %lf %lf %lf %lf %lf %lf\n", global_bounds[0], global_bounds[1], global_bounds[2], global_bounds[3], global_bounds[4], global_bounds[5]);
  }
  std::copy(global_bounds.begin(), global_bounds.end(), user_bounds);
#endif

  //double bounds[6] = {-0.083, 0.092, -0.51, 0.05, -0.51, 0.51};
  outputGrids =  set_origin_and_dim(user_bounds, grid_division);

  if (rank==0)
    printf("Global grid dim: %d %d %d (gd=%d)\n", outputGrids.dim[0], outputGrids.dim[1], outputGrids.dim[2], grid_division);

  ParallelResample parallelResample;
  parallelResample.GhostWidth = ghost_width;
  parallelResample.HistBinWidth = bin_width;
  parallelResample.DIYPrefix = prefix;
  parallelResample.DIYInMemory = in_memory;
  parallelResample.DIYThreads = threads;
  parallelResample.bUseVTKProbeFilter = false;
#ifdef PROFILING
  parallelResample.DebugWriteOutputData = false;
  parallelResample.DebugVerbose = (rank==0);
#else
  parallelResample.DebugWriteOutputData = true;
  parallelResample.DebugVerbose = true;
#endif

  parallelResample.run(world, data, outputGrids);

#ifdef DEMO
  world.barrier();

  if (rank!=0)
    return 0;

  {
    bool gridsOnly = true;
    dataList.clear();
    outputList.clear();
    int i;
    // load all inputs
    for (i = 0; i<world_size; i++)
      dataList.push_back( load_input(data_path, i, load_blocks, gridsOnly) );
    // load all outputs
    for (i = 0; i<world_size; i++)
    {
      vsp_new(vtkXMLImageDataReader, reader);
      stringstream ss;
      ss << "result_" << i << ".vti";
      reader->SetFileName(ss.str().c_str());
      reader->Update();
      outputList.push_back( reader->GetOutput() );
    }
  }

  printf("Keys:\n"
         "g: Toggle showing grids\n"
         "+/-: Increase/decrease grid size by 2\n"
         "<<< s: Simplify mesh >>>\n"
         "o: Toggle showing orginal model or simplified model\n"
         "v: Toggle using VTK simplification filter\n"
         "z: Save output data\n"
         );

  // Visualize
  vsp_new(vtkRenderer, ren);
  vsp_new(vtkRenderWindow, renWin);
  ::ren = ren.GetPointer();
  ::renWin = renWin.GetPointer();

  renWin->AddRenderer(ren);
  renWin->SetSize(800,600);

  vsp_new(vtkRenderWindowInteractor, renderWindowInteractor );
  renderWindowInteractor->SetRenderWindow(renWin);

  vsp_new(KeyPressInteractorStyle, style);
  style->SetCurrentRenderer(ren);
  renderWindowInteractor->SetInteractorStyle(style);

  draw();

  ren->ResetCamera();

  renWin->Render();

  renderWindowInteractor->Start();
#endif

  return EXIT_SUCCESS;

}
